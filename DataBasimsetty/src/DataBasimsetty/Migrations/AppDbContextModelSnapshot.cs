using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DataBasimsetty.Models;

namespace DataBasimsetty.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataBasimsetty.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<int?>("SectionSectionId");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("DataBasimsetty.Models.Section", b =>
                {
                    b.Property<int>("SectionId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FacultyId");

                    b.Property<int?>("LocationID");

                    b.Property<int>("capacity");

                    b.Property<int>("columns");

                    b.Property<int>("rows");

                    b.HasKey("SectionId");
                });

            modelBuilder.Entity("DataBasimsetty.Models.Location", b =>
                {
                    b.HasOne("DataBasimsetty.Models.Section")
                        .WithMany()
                        .HasForeignKey("SectionSectionId");
                });

            modelBuilder.Entity("DataBasimsetty.Models.Section", b =>
                {
                    b.HasOne("DataBasimsetty.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
