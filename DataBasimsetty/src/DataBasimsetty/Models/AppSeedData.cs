﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Collections.Generic;

namespace DataBasimsetty.Models
{
    public class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {

            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<AppDbContext>();

            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }

            context.Sections.RemoveRange(context.Sections);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedSectionsFromCsv(relPath, context);
        }

        private static void SeedSectionsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "section.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            Section.ReadAllFromCSV(source);
            List<Section> lst = Section.ReadAllFromCSV(source);
            context.Sections.AddRange(lst.ToArray());
            context.SaveChanges();
        }
        private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }
    }
}
   