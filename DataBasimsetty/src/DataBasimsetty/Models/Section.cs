﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace DataBasimsetty.Models
{
    public class Section
    {
        [ScaffoldColumn(false)]
        public int SectionId { get; set; }

        [Display(Name = "Faculty Name")]
        public string FacultyId { get; set; }

        [Display(Name = "capacity")]
        public int capacity { get; set; }

        [Display(Name = "rows")]
        public int rows { get; set; }

        [Display(Name = "columns")]
        public int columns { get; set; }

        [ScaffoldColumn(true)]
        public int? LocationID { get; set; }

        public virtual Location Location { get; set; }

        public List<Location> filmSets { get; set; }

        public static List<Section> ReadAllFromCSV(string filepath)
        {
            List<Section> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Section.OneFromCsv(v))
                                        .ToList();
            return lst;
        }
        public static Section OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Section item = new Section();

            int i = 0;
            item.FacultyId = Convert.ToString(values[i++]);
            item.capacity = Convert.ToInt32(values[i++]);
            item.rows = Convert.ToInt32(values[i++]);
            item.columns = Convert.ToInt32(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);


            return item;
        }
    }
}
